import {gql} from 'apollo-server';

export default gql` 
type Query {

    user(id: String!): User
    me: User

    feed(keyword: String, sort: SortInput): FeedPayload!
    post(id: String!): Post

    mailBox(filter: MailBoxFilter): MailBoxPayload!
    mail(id: String!): Mail
}

type Mutation {
    
    # Users
    createUser(authProvider: LinkedAccountInput!): User!
    signinUser(authProvider: LinkedAccountInput!): SigninPayload!
    signoutUser: Status!
    updateUser(
        id: String!,
        name: String,
        email: String,
        image: Upload,
    ): User!
    deleteUser(id: String!): Status!


    # Posts
    createPost(title: String!, description: String!, foundDate: Date!, image: Upload!): Post!
    updatePost(id: String!, title: String, description: String, foundDate: Date, image: Upload): Post!
    deletePost(id: String!): Status!


    # Evidence
    sendEvidence(postId: String!, message: String!, images:[Upload!]!): Status!
    acceptEvidence(id: String!): Status!
    refuseEvidence(id: String!): Status!
}

type User {
    id: ID!
    name: String!
    email: String!
    image: String!
    isOfficer: Boolean!
    createdPosts: [Post!]!
    createdEvidence: [Evidence!]!
}


type Post {
    id: ID!
    title: String!
    description: String!
    founder: User!
    foundDate: Date!
    postDate: Date!
    image: String!
}

type FeedPayload {
    posts: [Post!]!
    numOfPosts: Int!
}

input SortInput {
    by: SortFields!
    asc: Boolean
}

enum SortFields {
    foundDate
    postDate
}

type MailBoxPayload {
    mails: [Mail!]!
    numOfMails: Int!
}

enum MailBoxFilter {
    sent
    pending
    accepted
    refused
}

type Mail {
    id: ID!
    type: MailType!
    status: String
    post: Post!
    evidence: Evidence!
    sender: User
    isAccepted: Boolean
    date: Date!
}

enum MailType {
    acceptanceMail
    informMail
}

type Evidence {
    id: ID!
    message: String!
    images: [String!]!
}

input LinkedAccountInput {
    provider: AuthProviders!
    name: String
    # IF Email Provider
    email: String
    password: String
    # IF Other Providers Like Google
    id: ID
    idToken: String
    token: String
}

enum AuthProviders {
    email
    google
    facebook
}

type SigninPayload {
    user: User!
    accessToken: String!
    refreshToken: String!
}

enum Status {
    OK
    FAILED
}

scalar Date
`;