
import {  GraphQLDateTime } from 'graphql-iso-date';
import { defaultUserImage, defaultPostImage, defaultEvidenceImage } from '../lib/consts';
import { 
    AuthenticationError,
} from 'apollo-server';

export default {
    Query: {
        // Users
        user: (root, data, { models: { User }, user }) => User.findById(data.id, user),
        me: (root, data, { models: { User }, user }) => {
            if (!user.isLogedin) {
                throw new AuthenticationError('You must be logged in');
                // throw new ForbiddenError("You Don't Have Access");
            }

            return User.findById(user.id, user)
        },

        // Posts
        feed: (root, data, { models: { Post }, user }) => Post.feed(data, user),
        post: (root, data, { models: { Post }, user }) => Post.findById(data.id, user),

        // Mails
        mailBox: (root, data, { models: { Mail }, user }) => Mail.getMailBox(data, user),
        mail: (root, data, { models: { Mail }, user }) => Mail.findById(data.id, user),
    },
  
    Mutation: {
        // Users
        createUser: (root, data, { models: { User } }) => User.create(data),
        signinUser: (root, data, { models: { User } }) => User.login(data),
        signoutUser: (root, data, { models: { User }, user }) => User.signout(user),
        updateUser: (root, data, { models: { User }, user }) => User.update(data, user),
        deleteUser: (root, data, { models: { User }, user }) => User.delete(data, user),


        //Posts
        createPost: (root, data, { models: { Post }, user }) => Post.create(data, user),
        updatePost: (root, data, { models: { Post }, user }) => Post.update(data, user),
        deletePost: (root, data, { models: { Post }, user }) => Post.deleteById(data.id, user),

        // Evidences
        sendEvidence: (root, data, { models: { Evidence }, user }) => Evidence.create(data, user),
        acceptEvidence: (root, data, { models: { Evidence }, user }) => Evidence.decide(data.id, true, user),
        refuseEvidence: (root, data, { models: { Evidence }, user }) => Evidence.decide(data.id, false, user)
    },


    User: {
    id: root => root.id || root._id.toString(),
        image: root => root.image || defaultUserImage,
        createdPosts: (root, data, { models: { Post }, user }) => Post.findByCreatorId(root._id.toString(), user),
        createdEvidence: (root, data, { models: { Evidence }, user }) => Evidence.findByCreatorId(root._id.toString(), user)
    },

    Post: {
        id: root => root.id || root._id.toString(),
        image: root => root.image || defaultPostImage,
        founder: (root, data, { models: { User }, user }) => User.findById(root.founderId, user),
    },

    Mail: {
        id: root => root.id || root._id.toString(),
        post: (root, data, { models: { Post }, user }) => Post.findById(root.postId, user),
        evidence: (root, data, { models: { Evidence }, user }) => Evidence.findById(root.evidenceId, user),
        sender: (root, data, { models: { User }, user }) => User.findById(root.senderId, user),
        date: root => root.createdAt
    },

    Evidence: {
        id: root => root.id || root._id.toString(),
        images: root => root.images || [defaultEvidenceImage],
    },
  
    Date: GraphQLDateTime,
}
