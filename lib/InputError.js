
import { 
    UserInputError,
} from 'apollo-server';

// Generic form error class.  Allows simple error detection with .is(), and
// an ErrorType-compatible object with .errors()
export default class InputError {
    // Errors are stored on a plain object, in the form { fieldName: message }
    constructor() {
      this.validationErrors = {};
    }
  
    // Boolean to signal whether we have any errors
    is() {
      return !!Object.keys(this.validationErrors).length;
    }
  
    // Set an error by field name and error message
    set(field, message) {
      this.validationErrors[field] = message;
    }
    
    // Throws if we have a current error.  This will throw a GraphQL `ErrorType`
    // compatible object that can be caught further up the callstack
    throwIf() {
      if (this.is()) {
        throw new UserInputError(
            'Failed to get events due to validation errors',
            { validationErrors: this.validationErrors }
        );
      }
    }
  };
  