import cloudinary from 'cloudinary';
import fs from 'fs';


export const getJWT = (authorization) => {
    const temp = authorization.split(' ');
    if (temp.length == 2 && temp[0].toLowerCase() == 'bearer') {
      return temp[1];
    } else {
      return null;
    }
};

cloudinary.config({ 
  cloud_name: 'husseinraoouf', 
  api_key: '426512526756559', 
  api_secret: 'LX5V1h4VSSYSFpDKhSO0Nv1y6hM' 
});


export const isImage = (mimetype) => {
  return ['image/jpeg', 'image/tiff', 'image/gif', 'image/bmp', 'image/png'].includes(mimetype);
}

export const upload = (stream) => new Promise((resolve, reject) => {
  var writeStream = cloudinary.v2.uploader.upload_stream(function(error, result){ 
    if (error) reject(error);

    resolve(result.secure_url);
  });
  var file_reader = stream.pipe(writeStream);
});