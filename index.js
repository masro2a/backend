
if (process.env.NODE_ENV == 'development') {
  require('dotenv').config()
}

import { ApolloServer } from 'apollo-server';
import resolvers from './schema/resolvers';
import typeDefs from './schema/schema.js';
import connectDB from './db';
import { getJWT } from './lib/helpers';
import { decodeJWT } from './lib/hash';

const start = async () => {
  const dbModel = await connectDB();
  
  const buildContext = ({ req }) => {
    const authHeader = req.headers.authorization || req.headers.Authorization;
    console.log('authHeader', authHeader);
    const jwt = authHeader
      ? getJWT(authHeader)
      : null;
    const payload = jwt
      ? decodeJWT(jwt)
      : null;
    const user = payload
      ? Object.assign({ isLogedin: true }, payload, { accessToken: jwt })
      : { isLogedin: false };
  
    console.log('user', user);

    return { models: dbModel, user };
  };
  
  const server = new ApolloServer({ 
    typeDefs,
    resolvers,
    context: buildContext,
    playground: false,
    introspection: true
  });
  
  const PORT = process.argv[2] || 5000;
  server.listen({ port: PORT }).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
  });
}

start();
