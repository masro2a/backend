const nodeExternals = require('webpack-node-externals');
const path = require('path');
const webpack = require('webpack');
const NodemonPlugin = require( 'nodemon-webpack-plugin' ) // Ding
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    target: 'node',
    externals: [nodeExternals()],
    entry: './index.js',
    mode: 'development',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'inline-source-map',
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: false
        }),
        new CopyWebpackPlugin([
            'package.json',
            'Procfile'
        ]),     
        new NodemonPlugin(),
    ],
    module: {
        rules: [
            // {
            //     test: /\.m?js$/,
            //     include: /node_modules/,
            //     type: "javascript/auto"
            // },
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
        ]
    },
};