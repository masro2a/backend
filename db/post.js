
import isMongoId from 'validator/lib/isMongoId';

import {
    ObjectID,
} from 'mongodb';

import { 
  AuthenticationError,
  ForbiddenError,
  ApolloError,
} from 'apollo-server';

import InputError from '../lib/InputError';
import {isImage, upload} from '../lib/helpers';

export default ({
  Posts,
}) => {
  const methods = {};

  methods.findByCreatorId = (founderId) => Posts.find({founderId}).toArray().catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});

  methods.feed = async ({keyword, sort}, user) => {
    const query = {};

    if (keyword) {
      query['$or'] = [
        {title: { $regex: keyword, $options: 'i' }},
        {description: { $regex: keyword, $options: 'i' }},
      ]
    }

    let cursor = Posts.find(query);

    if (sort) {
      cursor = cursor.sort({[sort.by]: sort.asc == undefined ? true : sort.asc});
    }


    const result = await cursor.toArray().catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
  
    console.log(result);
    
    return {
      posts: result,
      numOfPosts: result.length
    }
  };

  methods.findById = (id) => {
    const e = new InputError();

    if(!isMongoId(id)) {
      e.set('id', 'Invalid id');
    }
    
    e.throwIf();

    return Posts.findOne({ _id: new ObjectID(id) }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
  };


  methods.create = async ({ title, description, foundDate, image }, user) => {
    const e = new InputError();
    
    if (!user.isLogedin) {
      throw new AuthenticationError('You must be logged in');
    }

    // Title
    if (title.length < 2 || title.length > 32) {
      e.set('name', 'Your name needs to be between 2-32 characters in length.');
    }

    if (description.length < 2 || description.length > 32) {
      e.set('desctiption', 'Your description needs to be between 2-32 characters in length.');
    }
    
    const { stream, filename, mimetype, encoding } = await image;

    if (!isImage(mimetype)) {
      e.set('image', 'It has to be image.');
    }

    e.throwIf();

    const imageUrl = await upload(stream);
    
    const newPost = {
      founderId: user.id,
      title: title,
      description: description,
      foundDate: foundDate,
      image: imageUrl
    };

    const response = await Posts.insertOne({ ...newPost, createdAt: new Date(), updatedAt: new Date() }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    return Object.assign(newPost, { id: response.insertedId.toString() });
  };

  methods.update = async ({id, title, description, foundDate, image}, user) => {
    const e = new InputError();

    if(!isMongoId(id)) {
      e.set('id', 'Invalid id');
    }
    
    e.throwIf();
    
    const post = await Posts.findOne({_id: new ObjectID(data.id)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    if (!post) {
      e.set('post', "doesn't exist");
    }

    e.throwIf();

    if (!user.isLogedin) {
      throw new AuthenticationError('You must be logged in');
    }
    
    if (user.id != post.founderId) {
      throw new ForbiddenError("You Don't Have Access");
    }
    
    /* Sanity check for input */
    if (!title && !description && !foundDate && !image) {
      e.set('update', 'Please provide a field to update.');
    }

    e.throwIf();


    const updateObject = {};

    if (title) {
      if (title.length < 2 || title.length > 32) {
        e.set('title', "Your name needs to be between 2-32 characters in length.");
      } else {
        updateObject.name = name;
      }
    }

    if (description) {
      if (description.length < 2 || description.length > 32) {
        e.set('description', "Your description needs to be between 2-32 characters in length.");
      } else {
        updateObject.description = description;
      }
    }

    if (foundDate) {
      updateObject.foundDate = foundDate;
    }

    if (image) {
      const { stream, filename, mimetype, encoding } = await image;
  
      if (!isImage(mimetype)) {
        e.set('image', 'It has to be image.');
      }

      e.throwIf();

      const imageUrl = await upload(stream);
      updateObject.image = imageUrl;
    }

    e.throwIf();

    return (await Posts.findAndModify(
      { _id: new ObjectID(id) }, 
      [], 
      { $set: { ...updateObject, updatedAt: new Date() } }, 
      { new:true },
    ).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})).value;
  };


  methods.deleteById = async (id, user) => {
    const e = new InputError();

    if(!isMongoId(id)) {
      e.set('id', 'Invalid id');
    }
    
    e.throwIf();

    const post = await Posts.findOne({_id: new ObjectID(id)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    if (!post) {
      e.set('podcast', 'doesn\'t exist')
    }

    e.throwIf();

    if (!user.isLogedin) {
      throw new AuthenticationError('You must be logged in');
    }
    
    if (user.id != post.founderId) {
      throw new ForbiddenError("You Don't Have Access");
    }
    
    const result = await Posts.removeOne({ _id: new ObjectID(id) }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    
    if (result.result.n = 1 && result.result.ok == 1)
      return 'OK';
    else
      return 'FAILED';
  }


  return methods;
};
