import {
  MongoClient,
} from 'mongodb';

import buildUser from './user';
import buildPost from './post';
import buildMail from './mail';
import buildEvidence from './evidence';

export default async () => {
  const client = await MongoClient.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
  const db = client.db(process.env.MONGODB_DB_NAME);
  const collections = {
    Users: db.collection('Users'),
    Posts: db.collection('Posts'),
    Mails: db.collection('Mails'),
    Evidences: db.collection('Evidences'),
};


return {
    _collections: collections,
    User: buildUser(collections),
    Post: buildPost(collections),
    Mail: buildMail(collections),
    Evidence: buildEvidence(collections),
  };
};
