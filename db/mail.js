
import isMongoId from 'validator/lib/isMongoId';

import {
    ObjectID,
} from 'mongodb';

import { 
  ApolloError,
} from 'apollo-server';

import InputError from '../lib/InputError';
import {officerMailBoxId} from '../lib/consts';

export default ({
    Mails,
}) => {
    const methods = {};

    methods.getMailBox = async ({ filter }, user) => {
        let query = {$or: [
            {reciverId: user.id}
        ]};

        if (user.isOfficer) {
            query['$or'].push({reciverId: officerMailBoxId});

            switch(filter) {
                case 'sent':
                    query = {senderId: user.id};
                    break;
                case 'pending':
                    query.type = 'informMail';
                    query.status = 'pending';
                    break;
                case 'accepted':
                    query.type = 'informMail';
                    query.status = 'accepted';
                    break;
                case 'refused':
                    query.type = 'informMail';
                    query.status = 'refused';
                break;
            }
        }


        const result = await Mails.find(query).toArray().catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
      
        return {
          mails: result,
          numOfMails: result.length
        }
    };
    
    methods.findById = (id) => {
        const e = new InputError();
    
        if(!isMongoId(id)) {
          e.set('id', 'Invalid id');
        }
        
        e.throwIf();
    
        return Mails.findOne({ _id: new ObjectID(id) }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    };

    return methods;
};