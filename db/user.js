import isEmail from 'validator/lib/isEmail';
import isMongoId from 'validator/lib/isMongoId';

import uuidv4 from 'uuid/v4';

import {
    ObjectID,
} from 'mongodb';

import { 
    AuthenticationError,
    ForbiddenError,
    ApolloError,
} from 'apollo-server';
import InputError from '../lib/InputError';
import {isImage, upload} from '../lib/helpers';

// Bcrypt hashing, for use with passwords
import { generatePasswordHash, checkPassword, encodeJWT } from '../lib/hash';

export default ({
    Users,
}) => {
    const methods = {};

    methods.findById = (id) => {
        const e = new InputError();

        if(!isMongoId(id)) {
            e.set('id', 'Invalid id');
        }
        
        e.throwIf();

        return Users.findOne({ _id: new ObjectID(id) }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    };


    methods.create = async ({ authProvider }) => {  
        const e = new InputError();
        let newUser;
    
        // Name
        if (!authProvider.name || authProvider.name.length < 2 || authProvider.name.length > 32) {
          e.set('name', 'Your name needs to be between 2-32 characters in length.');
        }
    
        if (authProvider.provider == 'email') {
          // E-mail
          if (!authProvider.email || !isEmail(authProvider.email)) {
            e.set('email', 'Please enter a valid e-mail.');
          } 
          
          else if ( (await Users.count({ linkedAccounts: { $elemMatch: { provider: 'email', email: authProvider.email }}}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})) > 0  ) {
            e.set('email', 'Your e-mail belongs to another account. Please login instead.');
          }
    
          // Password
          if (!authProvider.password || authProvider.password.length < 6 || authProvider.password.length > 64) {
            e.set('password', 'Please enter a password between 6 and 64 characters in length.');
          }
        } else {
          // Validate idToken
          if (!authProvider.idToken) {
            e.set('idToken', 'Please Enter Valid idToken');
          }
          
          if (!authProvider.id || (await Users.count({ linkedAccounts: { $elemMatch: { provider: authProvider.provider, id: authProvider.id }}}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})) > 0  ) {
            e.set('social account', 'There Is email with That social Account.');
          }
        }
    
        e.throwIf();
        
        // All good - proceed
        if (authProvider.provider == 'email') {
          newUser = {
            name: authProvider.name,
            email: authProvider.email,
            linkedAccounts: [
              {
                provider: 'email',
                email: authProvider.email,
                password: await generatePasswordHash(authProvider.password),
              }
            ],
            isOfficer: false,
          };
        } else {
          newUser = {
            name: authProvider.name,
            email: authProvider.email,
            linkedAccounts: [
              {
                provider: authProvider.provider,
                id: authProvider.id,
                email: authProvider.email,
                idToken: authProvider.idToken,
              }
            ],
            isOfficer: false,
          };
        }
        
        const response = await Users.insertOne({ ...newUser, createdAt: new Date(), updatedAt: new Date() }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        return Object.assign(newUser, { id: response.insertedId.toString() })
    };


    methods.login = async ({ authProvider }) => {

        const e = new InputError();
    
        if (authProvider.provider == 'email') {
          // E-mail
          if (!authProvider.email || !isEmail(authProvider.email)) {
            e.set('email', 'Please enter a valid e-mail.');
          } 
          
          // Password
          if (!authProvider.password || authProvider.password.length < 6 || authProvider.password.length > 64) {
            e.set('password', 'Please enter a password between 6 and 64 characters in length.');
          }
        } else {
          // Validate idToken
          if (!authProvider.idToken ) {
            e.set('idToken', 'Please Enter Valid idToken.');
          }
        }
    
        e.throwIf();
    
        let user;
    
        if (authProvider.provider == 'email') {
          user = await Users.findOne({ linkedAccounts: { $elemMatch: { provider: 'email', email: authProvider.email }}}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})
          
          if(!user) {
            e.set('email', 'There Is no account with that email.');
          }
          
          e.throwIf();
    
          const userPassword = user.linkedAccounts.find((account) => account.provider == 'email' && account.email == authProvider.email).password;
          if (!await checkPassword(authProvider.password, userPassword)) {
            e.set('password', 'Your password is incorrect. Please try again or click "forgot password".');
          }
    
          e.throwIf();
    
          const accessToken = encodeJWT({
            id: user._id,
            name: user.name,
            isOfficer: user.isOfficer
          })
    
          const refreshToken = uuidv4();
    
          await Users.updateOne({ _id: user._id},
            { $push: {
                sessions: {
                  refreshToken,
                  accessToken,
                  createdAt: new Date(),
                }
              }
            }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
          return {
            user,
            accessToken,
            refreshToken,
          };
        } else {
          user = await Users.findOne({ linkedAccounts: { $elemMatch: { provider: authProvider.provider, id: authProvider.id }}}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})

          if(!user) {
            e.set('social account', 'There Is no account with that email');
          }
          
          e.throwIf();
    
          const accessToken = encodeJWT({
            id: user._id,
            name: user.name,
            roles: user.roles,
          })
    
          const refreshToken= uuidv4();
    
          await Users.updateOne({ _id: user._id },
            { $push: {
                session: {
                  refreshToken,
                  accessToken,
                  createdAt: new Date(),
                }
              }
            }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
          return {
            user,
            accessToken,
            refreshToken,
          };
        }
    };


    methods.update = async (data, user) => {
        const e = new InputError();
        
        if(!isMongoId(data.id)) {
          e.set('id', 'Invalid id');
        }
        
        e.throwIf();
        
        const updateingUser = await Users.findOne({_id: new ObjectID(data.id)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        if (!updateingUser) {
          e.set('user', 'doesn\'t exist');
        }
    
        e.throwIf();
    
        if (!user.isLogedin) {
          throw new AuthenticationError('You must be logged in');
        }
        
        if (user.id != updateingUser._id) {
          throw new ForbiddenError("You Don't Have Access");
        }
    
    
        /* Sanity check for input */
        if (!data.name && !data.email && !data.image) {
          e.set('update', 'Please provide a field to update.');
        }
    
        e.throwIf();
    
        const updateObject = {};
        const arrayFilters = [];
    
        if (data.email) {
          if (!isEmail(data.email)) {
            e.set('email', 'Please enter a valid e-mail.');
          }      
          else if (await Users.findOne({ _id: {$ne: new ObjectID(data.id)}, linkedAccounts: { $elemMatch: { provider: 'email', email: data.email }}}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})) {
            e.set('email', 'Your e-mail belongs to another account. Please login instead.');
          } else {
            updateObject.email = data.email;
            updateObject["linkedAccounts.$[account].email"] = data.email;
            arrayFilters.push({ "account.provider": "email" })
          }
        }
    
        if (data.name) {
          if (data.name.length < 2 || data.name.length > 32) {
            e.set('name', 'Your name needs to be between 2-32 characters in length.');
          } else {
            updateObject.name = data.name;
          }
        }
        
        if (image) {
          const { stream, filename, mimetype, encoding } = await image;

          if (!isImage(mimetype)) {
            e.set('image', 'It has to be image.');
          }

          e.throwIf();

          const imageUrl = await upload(stream);
          
          updateObject.image = imageUrl;
        }

        e.throwIf();

        return (await Users.findAndModify(
          { _id: new ObjectID(data.id) }, 
          [], 
          { $set: { ...updateObject, updatedAt: new Date() } }, 
          { 
            new:true,
            arrayFilters,
          },
        ).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')})).value;
      }
    
    
      methods.signout = async (user) => {
        const result = await Users.updateOne(
          { _id: new ObjectID(user.id) }, 
          { $pull: { sessions: { accessToken: user.accessToken }}},
        ).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        
        if (result.result.n = 1 && result.result.ok == 1 )
          return 'OK';
        else
          return 'FAILED';
      }
    
    
      methods.delete = async ({id}, user) => {
        const e = new InputError();
    
        if(!isMongoId(id)) {
          e.set('id', 'Invalid id');
        }
        
        e.throwIf();
    
        updateingUser = await Users.findOne({_id: new ObjectID(id)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        if (!updateingUser) {
          e.set('user', 'doesn\'t exist');
        }
    
        e.throwIf();
    
        if (!user.isLogedin) {
          throw new AuthenticationError('You must be logged in');
        }
        
        if (user.id != updateingUser._id) {
          throw new ForbiddenError("You Don't Have Access");
        }
    
        
        const result = await Users.removeOne(
          { _id: new ObjectID(id) }, 
        ).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        
        if (result.result.n = 1 && result.result.ok == 1 )
          return 'OK';
        else
          return 'FAILED';
    }


    return methods;
};
