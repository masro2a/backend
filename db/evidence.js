
import isMongoId from 'validator/lib/isMongoId';

import {
    ObjectID,
} from 'mongodb';

import { 
  AuthenticationError,
  ApolloError,
} from 'apollo-server';

import InputError from '../lib/InputError';
import {isImage, upload} from '../lib/helpers';
import {officerMailBoxId} from '../lib/consts';

export default ({
    Evidences,
    Posts,
    Mails,
}) => {
    const methods = {};

    methods.findByCreatorId = (senderId) => Posts.find({senderId}).toArray().catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});

    methods.findById = (id) => {
        const e = new InputError();
    
        if(!isMongoId(id)) {
          e.set('id', 'Invalid id');
        }
        
        e.throwIf();
    
        return Evidences.findOne({ _id: new ObjectID(id) }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
    };

    methods.create = async ({ postId, message, images }, user) => {
        const e = new InputError();
        
        if (!user.isLogedin) {
          throw new AuthenticationError('You must be logged in');
        }

        if(!isMongoId(postId)) {
            e.set('postId', 'Invalid id');
        }
    
        // Title
        if (message.length < 2 || message.length > 400) {
          e.set('message', 'Your message needs to be between 2-400 characters in length.');
        }
    
        const post = await Posts.findOne({_id: new ObjectID(postId)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        if (!post) {
          e.set('post', 'doesn\'t exist');
        }

        const imagesUrl = [];
        for (const image of images) {
            const { stream, filename, mimetype, encoding } = await image;

        
            if (!isImage(mimetype)) {
              e.set('image', 'It has to be image.');
            }

            e.throwIf();
        
            imagesUrl.push(await upload(stream));
        }
    
        const evidenceId = new ObjectID();
        const newEvidence = {
            _id: evidenceId,
            message: message,
            images: imagesUrl,
            senderId: user.id,
            postId: postId
        };

        const newMail = {
            type: 'informMail',
            postId: postId,
            evidenceId: evidenceId.toString(),
            senderId: user.id,
            reciverId: officerMailBoxId,
            status: 'pending'
        }

        const mailsPromise = Mails.insertOne({ ...newMail, createdAt: new Date(), updatedAt: new Date() }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        const evidencesPromise = Evidences.insertOne({ ...newEvidence, createdAt: new Date(), updatedAt: new Date() }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});

        const result = await Promise.all([mailsPromise, evidencesPromise]);

        // return Object.assign(newEvidence, { id: result[1].insertedId.toString() });
        return 'OK';
    };



    methods.decide = async (evidenceId, isAccepted, user) => {
        const e = new InputError();
        
        if (!user.isLogedin) {
          throw new AuthenticationError('You must be logged in');
        }

        if(!isMongoId(evidenceId)) {
            e.set('id', 'Invalid id');
        }
        
        const evidence = await Evidences.findOne({_id: new ObjectID(evidenceId)}).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        if (!evidence) {
          e.set('evidence', 'doesn\'t exist');
        }
    
        e.throwIf();
        
        const newMail = {
            type: 'acceptanceMail',
            postId: evidence.postId,
            evidenceId: evidenceId,
            senderId: user.id,
            reciverId: evidence.senderId,
            isAccepted: isAccepted
        }
        
        await Mails.updateOne(
            { type: 'informMail', evidenceId: evidenceId},
            { $set: {
                    status: isAccepted ? 'accepted' : 'refused',
                    updatedAt: new Date()
                }
            }
        ).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});

        const response = await Mails.insertOne({ ...newMail, createdAt: new Date(), updatedAt: new Date() }).catch(e => { console.log(e); new ApolloError("database Failure", 'DATABASEFAILURE')});
        
        return 'OK';
    };

    return methods;
};